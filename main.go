package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)


func homePage(w http.ResponseWriter, r *http.Request) {
	//Allow CORS here By * or specific origin
	w.Header().Set("Access-Control-Allow-Origin", "*")
	//Endpoint Functions
	fmt.Fprintf(w, "Sergio Sebastian Chacón Herrera - 201709159")
}

func main() {
	go fmt.Println("Servidor en puerto 4000")

	//Endpoints
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/",homePage)
	
	//Levantar Servidor
	err:= http.ListenAndServe(":4000",myRouter)

	//Error
	if err!=nil {
		log.Fatal(err)
	}
}