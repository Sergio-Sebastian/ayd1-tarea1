package main

import (
	"fmt"
	"log"
	"net/http"
	"io/ioutil"
	"github.com/gorilla/mux"
	"encoding/json"
)

type nums struct {
	Num1 int `json:"num1"`
	Num2 int `json:"num2"`
}

func homePage(w http.ResponseWriter, r *http.Request) {
	//Allow CORS here By * or specific origin
	w.Header().Set("Access-Control-Allow-Origin", "*")
	//Endpoint Functions
	fmt.Fprintf(w, "Servidor Funcionando\nEsta es una calculadora con metodos POST en Golang")
}

func sumar(w http.ResponseWriter, r *http.Request) {
	//Allow CORS here By * or specific origin
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	//Endpoint Functions
	switch r.Method {
	case "POST":
		var numbers nums
		reqBody, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal([]byte(reqBody), &numbers)
		fmt.Println("Numeros: ", numbers)
		fmt.Fprintf(w, "Resultdao: %d", numbers.Num1+numbers.Num2)
	}
}

func restar(w http.ResponseWriter, r *http.Request) {
	//Allow CORS here By * or specific origin
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	//Endpoint Functions
	switch r.Method {
	case "POST":
		var numbers nums
		reqBody, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal([]byte(reqBody), &numbers)
		fmt.Println("Numeros: ", numbers)
		fmt.Fprintf(w, "Resultdao: %d", numbers.Num1-numbers.Num2)
	}
}

func main() {
	go fmt.Println("Servidor en puerto 3000")

	//Endpoints
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/",homePage)
	myRouter.HandleFunc("/sumar",sumar)
	myRouter.HandleFunc("/restar",restar)
	
	//Levantar Servidor
	err:= http.ListenAndServe(":3000",myRouter)

	//Error
	if err!=nil {
		log.Fatal(err)
	}
}